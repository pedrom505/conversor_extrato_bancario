from datetime import datetime
import Categorias
import Descricoes
import csv


def caixa(path: str):
    extrato = []
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        for index, row in enumerate(csv_reader):
            if index > 0:
                date_raw = row[1]
                date_obj = datetime.strptime(date_raw, '%Y%m%d')
                date = date_obj.strftime('%d/%m/%Y')

                ref = date_obj.strftime('%Y%m%d')

                description = row[3]
                value = row[4].replace('.', ',')
                if row[5] == 'D':
                    value = '-' + value
                if description.lower() not in ['saldo dia']:
                    extrato.append([ref, date, define_category(description), 'Caixa CC Pedro', define_description(description), value, 'Pago'])

    return extrato


def itau(path: str):
    extrato = []
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        for index, row in enumerate(csv_reader):
            date_raw = row[0]
            date_obj = datetime.strptime(date_raw, '%d/%m/%Y')
            date = date_obj.strftime('%d/%m/%Y')

            ref = date_obj.strftime('%Y%m%d')

            description = row[1]
            value = row[2].replace('.', ',')

            if description.lower() not in ["saldo final"]:
                extrato.append([ref, date, define_category(description), 'Itau CC Pedro', define_description(description), value, 'Pago'])

    return extrato


def inter(path: str):
    extrato = []
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        for index, row in enumerate(csv_reader):
            if index > 7:
                date_raw = row[0]
                date_obj = datetime.strptime(date_raw, '%d/%m/%Y')
                date = date_obj.strftime('%d/%m/%Y')

                ref = date_obj.strftime('%Y%m%d')

                description = row[1]
                description = description.replace('COMPRA CARTAO - COMPRA no estabelecimento ', '')

                value = row[2].replace(' ', '').replace('R$', '')

                extrato.append([ref, date, define_category(description), 'Inter CC Pedro', define_description(description), value, 'Pago'])

    return extrato


def nubank_cc(path: str):
    extrato = []
    with open(path, encoding="utf8") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for index, row in enumerate(csv_reader):
            if index > 1:
                date_raw = row[0]
                date_obj = datetime.strptime(date_raw, '%Y-%m-%d')
                date = date_obj.strftime('%d/%m/%Y')

                ref = date_obj.strftime('%Y%m%d')

                description = row[2]

                value = row[3].replace('.', ',')
                if '-' in value:
                    value = value.replace('-', '')
                else:
                    value = '-' + value

                reject_list = ['pagamento recebido', 'pagamento da fatura', 'pagamento de fatura']
                add_flag = True
                for reject_description in reject_list:
                    if reject_description in description.lower():
                        add_flag = False

                if add_flag:
                    extrato.append([ref, date, define_category(description), 'Nubank CC Pedro', 'Cartão de credito - ' + define_description(description), value, 'Pago'])

    return extrato


def nubank(path: str):
    extrato = []
    with open(path, encoding="utf8") as csv_file:

        csv_reader = csv.reader(csv_file, delimiter=',')
        for index, row in enumerate(csv_reader):
            if index > 1:
                date_raw = row[0]
                date_obj = datetime.strptime(date_raw, '%d/%m/%Y')
                date = date_obj.strftime('%d/%m/%Y')

                ref = date_obj.strftime('%Y%m%d')

                description = row[3]

                value = row[1].replace('.', ',')

                reject_list = ['pagamento da fatura', 'pagamento de fatura']
                add_flag = True
                for reject_description in reject_list:
                    if reject_description in description.lower():
                        add_flag = False

                if add_flag:
                    extrato.append([ref, date, define_category(description), 'Nubank CC Pedro', define_description(description), value, 'Pago'])

    return extrato


def sodexo(path: str):
    extrato = []
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        for index, row in enumerate(csv_reader):
            if index > 7:
                date_raw = row[0]
                date_obj = datetime.strptime(date_raw, '%d/%m')
                date_obj = date_obj.replace(year=datetime.today().year)
                date = date_obj.strftime('%d/%m/%Y')

                ref = date_obj.strftime('%Y%m%d')

                description = row[1]

                value = row[2]
                if 'DISPONIBILIZACAO' not in description:
                    value = '-' + value

                extrato.append([ref, date, define_category(description), 'Vale Sodexo Pedro', define_description(description), value, 'Pago'])

    return extrato


def define_category(description):
    description = description.lower()

    category_list = Categorias.categoria

    for category in category_list:
        for partial_description in category_list[category]:
            if partial_description.lower() in description:
                return category

    return ''


def define_description(description: str):

    description = description.lower()

    while '  ' in description:
        description = description.replace('  ', ' ')

    description_list = Descricoes.description

    for description_target in description_list:
        if description_target.lower() in description:
            return description_list[description_target]
    return description
