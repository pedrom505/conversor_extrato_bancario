description = {
    'deb cesta': 'Cesta de serviços',
    'di frango': 'Restaurante Di Frango',
    'freddo': 'Sorvete Freddo',
    'netflix.com': 'NetFlix',
    'estacioname': 'Estacionamento',
    'airsoft': 'Airsoft',
    'uber': 'Uber',
    '99app': '99 Taxi',
    'spotify': 'Spotify',
    'mercadolibre': 'Mercado Livre',
    'laggus': 'Pagamento Laggus - Casamento',
    'latam': 'Passagem aérea viagem',
    'net serv': 'Pagamento NET',
    'steam': 'Compra de jogo na Steam',
    'gustavo paul': 'Cirurgia Dr. Gustavo',
    'pista ': 'Pedagio'
}
