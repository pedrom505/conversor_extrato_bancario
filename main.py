from datetime import datetime

import os
import csv
import Conversor


def sort_date(e):
    return e[0]


lower_date = '22/04/2022'


lower_date_obj = datetime.strptime(lower_date, '%d/%m/%Y')
extrato_completo = []

for (dirpath, dirnames, filenames) in os.walk("Extratos"):
    for file in filenames:
        full_path = os.path.join(dirpath, file)
        if 'caixa' in full_path.lower():
            extrato_completo.extend(Conversor.caixa(full_path))
        if 'itau' in full_path.lower():
            extrato_completo.extend(Conversor.itau(full_path))
        if 'inter' in full_path.lower():
            extrato_completo.extend(Conversor.inter(full_path))
        if 'nubank' in full_path.lower():
            extrato_completo.extend(Conversor.nubank_cc(full_path))
        if 'nu_520790965' in full_path.lower():
            extrato_completo.extend(Conversor.nubank(full_path))
        if 'sodexo' in full_path.lower():
            extrato_completo.extend(Conversor.sodexo(full_path))

extrato_completo.sort(key=sort_date)

extrato_filtered = []
for item in extrato_completo:
    item_date = item[1]
    item_date_obj = datetime.strptime(item_date, '%d/%m/%Y')

    if item_date_obj >= lower_date_obj:
        extrato_filtered.append(item)


with open('output.csv', mode='w', newline='', encoding="utf8") as file:
    employee_writer = csv.writer(file, delimiter=';')

    for item in extrato_filtered:
        employee_writer.writerow(item[1::])

